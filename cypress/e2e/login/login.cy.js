/// <reference types="cypress"/>

import Login from "../../support/page-object/login/login.po";
import Properties from "../../support/page-object/properties/properties.po";
describe("Login Module", () => {
  const login = new Login();
  const properties = new Properties();

  // Valid login test case
    it("Valid login test scenario", () => {
      cy.visit("");
      cy.url({ timeout: 20000 }).should(
        "eq",
        Cypress.config("baseUrl") + "/login?returnUrl=%2F"
      );
      cy.fixture("userdata").then((data) => {
        login.Username().type(data.valid_userdata[0].username);
        login.Password().type(data.valid_userdata[0].password);
        login.LoginBtn().click();
        cy.url({ timeout: 15000 }).should(
          "eq",
          Cypress.config("baseUrl") +
            "/crm/properties?page=1&limit=15&sort=-Properties.created"
        );
        properties.PropertyHead().should("contain.text", "Properties");
    });
  });

  // Invalid login test cases
    it("Invalid username with invalid password scenario", () => {
      cy.visit("");
      cy.url({ timeout: 20000 }).should(
        "eq",
        Cypress.config("baseUrl") + "/login?returnUrl=%2F"
      );
      cy.fixture("userdata").then((data) => {
        login.Username().type(data.invalid_userdata[1].username);
        login.Password().type(data.invalid_userdata[1].password);
        login.LoginBtn().click();
        login
          .ToastMessage()
          .should("contain.text", "Invalid username or password");
      });
    });

    it("Valid username with invalid password scenario", () => {
      cy.visit("");
      cy.url({ timeout: 20000 }).should(
        "eq",
        Cypress.config("baseUrl") + "/login?returnUrl=%2F"
      );
      cy.fixture("userdata").then((data) => {
        login.Username().type(data.valid_userdata[0].username);
        login.Password().type(data.invalid_userdata[1].password);
        login.LoginBtn().click();
        login
          .ToastMessage()
          .should("contain.text", "Invalid username or password");
      });
    });

    it("Valid password with invalid username scenario", () => {
      cy.visit("");
      cy.url({ timeout: 20000 }).should(
        "eq",
        Cypress.config("baseUrl") + "/login?returnUrl=%2F"
      );
      cy.fixture("userdata").then((data) => {
        login.Username().type(data.invalid_userdata[1].username);
        login.Password().type(data.valid_userdata[0].password);
        login.LoginBtn().click();
        login
          .ToastMessage()
          .should("contain.text", "Invalid username or password");
      });
    });
  });
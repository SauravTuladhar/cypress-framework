/// <reference types="cypress"/>

import Properties from "../../support/page-object/properties/properties.po";

describe("Properties Module", () => {
  const properties = new Properties();
  beforeEach(() => {
    cy.Login();
  });

  it("Search in properties", () => {
    cy.fixture("userdata").then((data) => {
      properties.Search().type(data.properties_Data[0].search);
      properties.ClickSearch().click();
      properties.ValidateSearch().should("contain.text", "Two storey");
    });
  });

  it("Add properties", () => {
    cy.fixture("userdata").then((data) => {
      properties.AddButton().click();
      properties.ClickSearch().click();
      properties
        .ValidatePropertySlider()
        .should("contain.text", " Create a new Property");
      properties
        .ShortDescription()
        .type(data.properties_Data[0].shortDescription);
      properties.SelectPropertyType().click();
      properties.SelectHouse().click();
      properties.SaveProperty().click();
      properties.ClosePanel().click();
      cy.wait(5000);
      properties.SelectCreatedProperty().click();
      properties
        .ValidateCreatedProperty()
        .should("contain.text", " This is short description ");
    });
  });

  it("Delete properties", () => {
    cy.fixture("userdata").then((data) => {
      properties.DeleteProperty().click();
      properties.ConfirmDelete().click();
    });
  });
});

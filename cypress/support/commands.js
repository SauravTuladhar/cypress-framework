// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
import Login from "./page-object/login/login.po";
import Properties from "./page-object/properties/properties.po";
Cypress.Commands.add("Login", () => {
  const login = new Login();
  const properties = new Properties();
  cy.visit("");
  cy.url({ timeout: 20000 }).should(
    "eq",
    Cypress.config("baseUrl") + "/login?returnUrl=%2F"
  );
  cy.fixture("userdata").then((data) => {
    login.Username().type(data.valid_userdata[0].username);
    login.Password().type(data.valid_userdata[0].password);
    login.LoginBtn().click();
    cy.url({ timeout: 20000 }).should(
      "eq",
      Cypress.config("baseUrl") +
        "/crm/properties?page=1&limit=15&sort=-Properties.created"
    );
    properties.PropertyHead().should("contain.text", "Properties");
  });
});

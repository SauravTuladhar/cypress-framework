/// <reference types="cypress"/>

class Properties {
  PropertyHead() {
    return cy
      .contains("span", "Properties", { timeout: 10000 })
      .should("exist")
      .should("be.visible");
  }

  Search() {
    return cy
      .get('input[placeholder="Search in Properties"]', { timeout: 10000 })
      .should("exist")
      .should("be.visible");
  }

  ClickSearch() {
    return cy
      .get('button[form="globalSearchForm"]', { timeout: 10000 })
      .should("exist")
      .should("be.visible");
  }

  ValidateSearch() {
    return cy
      .contains("div", "Two storey", { timeout: 10000 })
      .should("exist")
      .should("be.visible");
  }

  AddButton() {
    return cy
      .contains("button", "Add", { timeout: 10000 })
      .should("exist")
      .should("be.visible");
  }

  ValidatePropertySlider() {
    return cy
      .contains("span", " Create a new Property", { timeout: 10000 })
      .should("exist")
      .should("be.visible");
  }

  ShortDescription() {
    return cy
      .xpath('(//qobrix-form-input[@formcontrolname="short_description"])[1]', {
        timeout: 10000,
      })
      .should("exist")
      .should("be.visible");
  }

  SelectPropertyType() {
    return cy
      .xpath('(//qobrix-multi-select[@formcontrolname="property_type"])[1]', {
        timeout: 10000,
      })
      .should("exist")
      .should("be.visible");
  }

  SelectHouse() {
    return cy
      .contains("span", "House", { timeout: 10000 })
      .should("exist")
      .should("be.visible");
  }

  SelectCreatedProperty() {
    return cy
      .xpath('//img[@alt="House"]', { timeout: 10000 })
      .should("exist")
      .should("be.visible");
  }

  SaveProperty() {
    return cy
      .xpath('//button[contains(text(),"Save")]', { timeout: 10000 })
      .should("exist")
      .should("be.visible");
  }

  ClosePanel() {
    return cy
      .xpath('//button[@title="Close Panel"]', { timeout: 10000 })
      .should("exist")
      .should("be.visible");
  }

  ValidateCreatedProperty() {
    return cy
      .xpath('(//div[contains(text()," This is short description ")])[1]', {
        timeout: 10000,
      })
      .should("exist")
      .should("be.visible");
  }

  DeleteProperty() {
    return cy
      .xpath('(//a[@title="Delete"])[1]', { timeout: 10000 })
      .should("exist")
      .should("be.visible");
  }

  ConfirmDelete() {
    return cy
      .contains("button", " Yes, I'm sure ", { timeout: 10000 })
      .should("exist")
      .should("be.visible");
  }
}

export default Properties;

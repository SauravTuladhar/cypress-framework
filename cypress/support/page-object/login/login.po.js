/// <reference types="cypress"/>

class Login {
  Username() {
    return cy
      .get("input[type=email]", { timeout: 10000 })
      .should("exist")
      .should("be.visible");
  }

  Password() {
    return cy
      .get("input[type=password]", { timeout: 10000 })
      .should("exist")
      .should("be.visible");
  }

  LoginBtn() {
    return cy
      .get("button[type=submit]", { timeout: 10000 })
      .should("exist")
      .should("be.visible");
  }

  ToastMessage() {
    return cy
      .get("div[class='mt-6 mb-6 text-base text-center text-red-500']", {
        timeout: 20000,
      })
      .should("exist")
      .should("be.visible");
  }
}

export default Login;
